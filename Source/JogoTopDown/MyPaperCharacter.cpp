// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPaperCharacter.h"


AMyPaperCharacter::AMyPaperCharacter()
{
	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	cam->ProjectionMode = ECameraProjectionMode::Orthographic;

	CharMovementComp = GetCharacterMovement();
}

void AMyPaperCharacter::MoveForward(float Value)
{
	FVector forward = FVector(0,0,1);

	AddMovementInput(forward, 1.0f, false);
}

void AMyPaperCharacter::MoveRight(float Value)
{
	FVector right = FVector(1, 0, 0);

	AddMovementInput(right, 1.0f, false);
}
