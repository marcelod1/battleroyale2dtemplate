// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "Camera/CameraComponent.h"

#include "MyPaperCharacter.generated.h"

class UCharacterMovementComponent;
/**
 * 
 */
UCLASS()
class JOGOTOPDOWN_API AMyPaperCharacter : public APaperCharacter
{
	GENERATED_BODY()
public:
	AMyPaperCharacter();
	

private:
		UPROPERTY()
		UCameraComponent* cam;

		UPROPERTY()
		UCharacterMovementComponent* CharMovementComp;

		void MoveForward(float Value);

		void MoveRight(float Value);
		
};
