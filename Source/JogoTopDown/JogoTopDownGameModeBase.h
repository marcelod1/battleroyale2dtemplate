// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JogoTopDownGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JOGOTOPDOWN_API AJogoTopDownGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
